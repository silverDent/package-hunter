'use strict'

const basicAuth = require('express-basic-auth')
const bcrypt = require('bcrypt')
const User = require('./user')
const debug = require('debug')('pkgs:authorizer')

module.exports = function (store, hashfunc = bcrypt) {
  const users = User.getAllUsers(store)

  return async function (username, password, cb) {
    try {
      for (const user of users) {
        const userMatches = basicAuth.safeCompare(username, user.name)
        const passwordMatches = await hashfunc.compare(decodeURIComponent(password), user.hash)
        if (userMatches & passwordMatches) { return cb(null, true) }
      }
      debug(`failed auth attempt for ${username}`)
      return cb(null, false)
    } catch (err) {
      return cb(err, null)
    }
  }
}
